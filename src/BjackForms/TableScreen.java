/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BjackForms;

import bjackclient.ServerConnection;
import classeslibrary.*;
import java.awt.Color;
import static java.awt.Component.CENTER_ALIGNMENT;
import java.awt.Cursor;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

/**
 *
 * @author red week
 */
public class TableScreen extends javax.swing.JFrame {
    
    Data data = new Data();
    Player player;
    Table table = new Table();
    ServerConnection server = new ServerConnection();
    
    JTextField txtBet = new JTextField();
    Border b = new BevelBorder(1, Color.BLACK, Color.BLACK); 
    
    //Creates new form TableScreen
    public TableScreen() {
        initComponents();
    }
    
    //Creates new form TableScreen with the needed informtion
    public TableScreen(Player p, Data d, Table t) {
        
        initComponents();
        this.getContentPane().setBackground(Color.green); //paint the frame back ground
        this.setLocationRelativeTo(null);   //place the form in the center
        //get the classes from the privios page
        this.player = new Player(p);
        this.data = new Data(d);
        this.table = new Table(t);
        //set grid for the cards panels
        GridLayout pnlGrid = new GridLayout(0,3);
        pnlDealer.setLayout(pnlGrid);       //layout to dealer
        pnlPlayer.setLayout(pnlGrid);       //layout to player
        //show the coins to the player
        lbl_coins.setText(" " + player.getAmount());
        //show the number of the table
        lbl_tableNumber.setText("" + table.getId());
        //if player need to place a bet
        if ("bet".equals(data.getServerMsg())) {
            //fill the bet panel
            //panel
            pnlBet.setBorder(b);
            pnlBet.setLayout(new GridLayout(3, 0));
            //label
            JLabel lblBet = new JLabel("SET YOUR BET");
            lblBet.setHorizontalAlignment((int) CENTER_ALIGNMENT);
            pnlBet.add(lblBet);
            //txt
            pnlBet.add(txtBet);
            //button
            JButton btnBet = new JButton("BET");                            //create new button
            btnBet.setHorizontalAlignment((int) CENTER_ALIGNMENT);          //set the words to apaer in the center
            btnBet.setCursor(new Cursor(12));                               //set the mouse to apaer as hand
            btnBet.addMouseListener(new java.awt.event.MouseAdapter() {     //make a mouse clicked for the button
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    btnBetClicked(evt);
                }
            });
            pnlBet.add(btnBet);          //plase the button on the panel
        }
    }
    /*
        set card in the right panel
        if need to put more then 3 cards in the panel - open the scroll
        (needed for the size of the icon)
    */
    public void setCard(String name, JPanel panel, int cardsOnBoard){
        if(cardsOnBoard >= 3){
            panel.setLayout(new BoxLayout(panel,BoxLayout.X_AXIS));
        }
        
        pnlCard card = new pnlCard();   //create card panel
        
        if(!"XX".equals(name)){         //if its not a upside down card 
        
            String shape;
            String digit;
            shape = name.substring(name.length() - 1);      //hold the shape of the card
            digit = name.substring(0, name.length() - 1);    //hold the digit of the card
            
            //make the right icon and color for the card
            switch (shape) {
                case "D":
                    card.lblCard.setIcon(new ImageIcon(getClass().getResource("/cardIcons/di.png")));
                    card.lblCard.setText(digit);        //set the name of the card on the card
                    card.lblCard.setForeground(Color.red);
                    break;
                case "H":
                    card.lblCard.setIcon(new ImageIcon(getClass().getResource("/cardIcons/he.png")));
                    card.lblCard.setText(digit);        //set the name of the card on the card
                    card.lblCard.setForeground(Color.red);
                    break;
                case "L":
                    card.lblCard.setIcon(new ImageIcon(getClass().getResource("/cardIcons/le.png")));
                    card.lblCard.setText(digit);        //set the name of the card on the card
                    break;
                case "T":
                    card.lblCard.setIcon(new ImageIcon(getClass().getResource("/cardIcons/ti.png")));
                    card.lblCard.setText(digit);        //set the name of the card on the card
                    break;
                default:
                    System.err.println("shape not recognize");
            }
            
            //make possible to write on top of the card
            card.lblCard.setHorizontalTextPosition(JLabel.CENTER);
            card.lblCard.setVerticalTextPosition(JLabel.CENTER);
        }else{
            //make the hidden card 
            card.lblCard.setIcon(new ImageIcon(getClass().getResource("/cardIcons/xx.png")));
        }
        
        panel.add(card);                 //add the card to the panel
        panel.revalidate();                   
        panel.repaint();
    }
    
    public void dealerTurn(){
        //start the player turn
        //show the down side card
        pnlDealer.removeAll();
        pnlDealer.revalidate();                   
        pnlDealer.repaint();
        setCard(table.getDealerCard(0).getName(), pnlDealer, 1);
        setCard(table.getDealerCard(1).getName(), pnlDealer, 2);
        //if player lost or got Bjack jump on it
        if (!"lose".equals(player.getStatus()) && !"win".equals(player.getStatus())) {
            if (table.cardsSum() == 21) {     //if dealer got Bjack 
                //everyone losses
                //lbl_clientMsg.setText();
                msg("dealer got Bjack!! you lost!!!");
                //take the money from the player
                data.setClientMsg("lose");
                data.setPlayer(player);
                data.setTable(table);                           //set the table in data for the hand insert
                server.createConnection();                      //create connection with the server
                server.setObjectToServer(data);                 //send the data to the server
                data = (Data) server.getObjectFromServer();     //get the data from the server back
                server.closeConnection();                       //close connection with the server
                player = data.getPlayer();
                table = data.getTable();
            } else {
                //if dealer got less then 17 points
                while (table.cardsSum() < 17) {
                    //dealer gets more cards
                    data.setClientMsg("get dealer card");
                    data.setTable(table);
                    server.createConnection();                      //create connection with the server
                    server.setObjectToServer(data);
                    data = (Data) server.getObjectFromServer();
                    server.closeConnection();                       //close connection with the server
                    table = data.getTable();
                    //show the dealer new card
                    int size = table.getDealerCards().size();
                    setCard(table.getDealerCard(size - 1).getName(), pnlDealer, size);
                }
                if (table.cardsSum() > 21) {
                    //dealer lose everyone wins
                    //lbl_clientMsg.setText();
                    msg("dealer lose!! you won!!!");
                    player.win();
                    data.setClientMsg("win");
                    data.setPlayer(player);
                    data.setTable(table);
                    server.createConnection();                      //create connection with the server
                    server.setObjectToServer(data);
                    data = (Data) server.getObjectFromServer();
                    server.closeConnection();                       //close connection with the server
                    player = data.getPlayer();
                    table = data.getTable();
                } else {
                    //round over check result
                    if (table.cardsSum() > player.cardsSum()) {
                        //dealer high then player = player lose
                        //lbl_clientMsg.setText();
                        msg("dealer won!!! you lost!!!");
                        //maybe need player.lose();
                        data.setClientMsg("lose");
                    } else if (table.cardsSum() < player.cardsSum()) {
                        //player high then dealer = player win
                        //lbl_clientMsg.setText();
                        msg("you won!!!");
                        player.win();
                        data.setClientMsg("win");
                    } else {
                        //even = player get his money back
                        //lbl_clientMsg.setText();
                        msg(" you are even ");
                        player.even();
                        data.setClientMsg("even");
                    }
                    data.setPlayer(player);
                    data.setTable(table);                           //set the table in data for the hand insert
                    server.createConnection();                      //create connection with the server
                    server.setObjectToServer(data);                 //send the data to the server
                    data = (Data) server.getObjectFromServer();     //get the data from the server back
                    server.closeConnection();                       //close connection with the server
                    player = data.getPlayer();
                    table = data.getTable();
                }
            }
        }
        //update the coins for the player
        lbl_coins.setText(" " + player.getAmount());
        player.resetRound();            //reset the round info to the player
        table.resetDealerCards();       //reset the dealer cards
        
        //make new game button
        JButton btnNewGame = new JButton("NEW GAME");
        btnNewGame.setHorizontalAlignment((int) CENTER_ALIGNMENT);
        btnNewGame.setCursor(new Cursor(12));                           //set the mouse to apaer as hand
        btnNewGame.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnNewGameClicked(evt);
            }
        });
        pnlNewGame.setLayout(new GridLayout(1, 1));
        pnlNewGame.add(btnNewGame);
        
        this.revalidate();
        this.repaint();
    }
    
    public void msg(String msg){
        Thread thread = new Thread(){
            @Override
            public void run(){
                lbl_clientMsg.setText(msg);
                //Thread.sleep(3000);
                //lbl_clientMsg.setText("round is over");
            }
        };
        thread.start();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btn_quit = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lbl_coins = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        pnlBet = new javax.swing.JPanel();
        pnlCommends = new javax.swing.JPanel();
        spnl_dealer = new javax.swing.JScrollPane();
        pnlDealer = new javax.swing.JPanel();
        lbl_clientMsg = new javax.swing.JLabel();
        pnlNewGame = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lbl_tableNumber = new javax.swing.JLabel();
        spnl_player = new javax.swing.JScrollPane();
        pnlPlayer = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 255, 0));
        setForeground(new java.awt.Color(0, 255, 0));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_quit.setBackground(new java.awt.Color(255, 0, 0));
        btn_quit.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btn_quit.setText("QUIT");
        btn_quit.setToolTipText("");
        btn_quit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_quit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_quitMouseClicked(evt);
            }
        });
        getContentPane().add(btn_quit, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 540, -1, 39));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lbl_coins.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lbl_coins.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("YOUR COINS :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbl_coins, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbl_coins, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(657, 11, -1, -1));

        pnlBet.setBackground(new java.awt.Color(0, 255, 0));

        javax.swing.GroupLayout pnlBetLayout = new javax.swing.GroupLayout(pnlBet);
        pnlBet.setLayout(pnlBetLayout);
        pnlBetLayout.setHorizontalGroup(
            pnlBetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        pnlBetLayout.setVerticalGroup(
            pnlBetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        getContentPane().add(pnlBet, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 277, -1, -1));

        pnlCommends.setBackground(new Color(0,0,0,0));

        javax.swing.GroupLayout pnlCommendsLayout = new javax.swing.GroupLayout(pnlCommends);
        pnlCommends.setLayout(pnlCommendsLayout);
        pnlCommendsLayout.setHorizontalGroup(
            pnlCommendsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 125, Short.MAX_VALUE)
        );
        pnlCommendsLayout.setVerticalGroup(
            pnlCommendsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 177, Short.MAX_VALUE)
        );

        getContentPane().add(pnlCommends, new org.netbeans.lib.awtextra.AbsoluteConstraints(627, 393, -1, -1));

        spnl_dealer.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        pnlDealer.setBackground(new java.awt.Color(255, 255, 255));
        pnlDealer.setToolTipText("");
        pnlDealer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        spnl_dealer.setViewportView(pnlDealer);

        getContentPane().add(spnl_dealer, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 62, 504, 186));

        lbl_clientMsg.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        getContentPane().add(lbl_clientMsg, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 254, 342, 139));

        pnlNewGame.setBackground(new Color(0,0,0,0));

        javax.swing.GroupLayout pnlNewGameLayout = new javax.swing.GroupLayout(pnlNewGame);
        pnlNewGame.setLayout(pnlNewGameLayout);
        pnlNewGameLayout.setHorizontalGroup(
            pnlNewGameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 75, Short.MAX_VALUE)
        );
        pnlNewGameLayout.setVerticalGroup(
            pnlNewGameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 75, Short.MAX_VALUE)
        );

        getContentPane().add(pnlNewGame, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 393, -1, -1));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("TABLE :");

        lbl_tableNumber.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbl_tableNumber.setText("0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_tableNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_tableNumber, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 0, -1, -1));

        spnl_player.setBackground(new Color(0,0,0,0));
        spnl_player.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        pnlPlayer.setBackground(new java.awt.Color(255, 255, 255));
        pnlPlayer.setToolTipText("");
        pnlPlayer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        spnl_player.setViewportView(pnlPlayer);

        getContentPane().add(spnl_player, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 393, 504, 186));

        jLabel3.setBackground(new Color(0,0,0,0));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/backgrounds/background1.jpg"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(-6, 0, 780, 610));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnNewGameClicked(java.awt.event.MouseEvent evt) {
        
        TableScreen tableScreen = new TableScreen(this.player, this.data, this.table);
        tableScreen.setVisible(true);
        this.dispose();
    }
    
    private void btnBetClicked(java.awt.event.MouseEvent evt) {
        lbl_clientMsg.setText("");
        try {
            int bet = Integer.valueOf(txtBet.getText());
            if (player.firstBet(bet) == 1) {                    //update the first bet
                
                //get the cards
                server.createConnection();                      //connect to the server
                data.setClientMsg("bet");                       //tell the server to take the bet
                data.setPlayer(player);                         //take the player into the data
                data.setTable(table);                           //take the table into the data
                server.setObjectToServer(data);                 //send the data to the server
                data = (Data) server.getObjectFromServer();     //get the data from the server back
                server.closeConnection();                       //close connection with the server
                //player and dealer gets their first cards
                player = data.getPlayer();
                table = data.getTable();
                
                //show the coins the player
                lbl_coins.setText(" " + player.getAmount());    //update the new amount
                //remove the bet panel
                pnlBet.removeAll();
                pnlBet.revalidate();
                pnlBet.repaint();
                //show the first cards of player
                for(int index = 0; index < player.getCards().size(); index++){
                    setCard(player.getCard(index).getName(), pnlPlayer, index + 1);     //cards of player
                }
                //show the first cards of dealer
                setCard(table.getDealerCard(0).getName(), pnlDealer, 1);
                setCard("XX", pnlDealer, 2);
                
                if (data.getAnswer() == 1){     //if Bjack
                    //massage the client and update the coins
                    lbl_clientMsg.setText("Bjack! you won!!!");
                    lbl_coins.setText(" " + player.getAmount());
                    //end your turn
                    dealerTurn();
                }else{
                    //add commends buttons
                    pnlCommends.setBorder(b);
                    pnlCommends.setLayout(new GridLayout(3, 0));
                    JButton btnPass = new JButton("PASS");
                    btnPass.setHorizontalAlignment((int) CENTER_ALIGNMENT);
                    btnPass.setCursor(new Cursor(12));                          //set the mouse to apaer as hand
                    btnPass.addMouseListener(new java.awt.event.MouseAdapter() {
                        @Override
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                            btnPassClicked(evt);
                        }
                    });
                    JButton btnOneMoreCard = new JButton("CARD");
                    btnOneMoreCard.setHorizontalAlignment((int) CENTER_ALIGNMENT);
                    btnOneMoreCard.setCursor(new Cursor(12));                   //set the mouse to apaer as hand
                    btnOneMoreCard.addMouseListener(new java.awt.event.MouseAdapter() {
                        @Override
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                            btnOneMoreCardClicked(evt);
                        }
                    });
                    JButton btnDouble = new JButton("DOUBLE");
                    btnDouble.setHorizontalAlignment((int) CENTER_ALIGNMENT);
                    btnDouble.setCursor(new Cursor(12));                        //set the mouse to apaer as hand
                    btnDouble.addMouseListener(new java.awt.event.MouseAdapter() {
                        @Override
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                            btnDoubleClicked(evt);
                        }
                    });
                    pnlCommends.add(btnPass);
                    pnlCommends.add(btnOneMoreCard);
                    if(player.getAmount() >= player.getFirstBet()){
                        pnlCommends.add(btnDouble);
                    }   
                }
            } else {
                if(bet == 0){
                    lbl_clientMsg.setText("you can't bet nothing");
                }else{
                    lbl_clientMsg.setText("you don't have enough money");
                }
            }
        } catch (NumberFormatException ex) {
            lbl_clientMsg.setText("enter only numbers you trouble maker");
        }
    }
    
    private void btnPassClicked(java.awt.event.MouseEvent evt) {
        player.card();
        
        pnlCommends.removeAll();
        pnlCommends.revalidate();
        pnlCommends.repaint();
        this.revalidate();
        this.repaint();
        
        //end the turn
        dealerTurn();
    }
    
    private void btnOneMoreCardClicked(java.awt.event.MouseEvent evt) {
        data.setClientMsg("one more card");
        data.setPlayer(player);                         //take the player into the data
        data.setTable(table);                           //take the table into the data
        server.createConnection();                      //create connection with the server
        server.setObjectToServer(data);                 //send the data to the server
        data = (Data) server.getObjectFromServer();     //get the data from the server back
        server.closeConnection();                       //close connection with the server
        //player gets the card
        player = data.getPlayer();
        table = data.getTable();
        
        //show the player new card
        int size = player.getCards().size();
        setCard(player.getCard(size - 1).getName(), pnlPlayer, size);
        
        if (data.getAnswer() == 0) {    //player lose
            lbl_clientMsg.setText("you lost");
            //update the coins for the player
            lbl_coins.setText(" " + player.getAmount());
            pnlCommends.removeAll();
            pnlCommends.revalidate();
            pnlCommends.repaint();
            this.revalidate();
            this.repaint();
            //end your turn
            dealerTurn();
        }
    }
    
    private void btnDoubleClicked(java.awt.event.MouseEvent evt) {
        if (player.getCards().size() == 2) {
            data.setClientMsg("double");
            data.setPlayer(player);                         //take the player into the data
            data.setTable(table);                           //take the table into the data
            server.createConnection();                      //create connection with the server
            server.setObjectToServer(data);                 //send the data to the server
            data = (Data) server.getObjectFromServer();     //get the data from the server back
            server.closeConnection();                       //close connection with the server
            player = data.getPlayer();                      //player gets the card
            table = data.getTable();

            //update the coins
            lbl_coins.setText(" " + player.getAmount());

            //show the player new card
            int size = player.getCards().size();
            setCard(player.getCard(size - 1).getName(), pnlPlayer, size);

            if (data.getAnswer() == 0) {  //player lose
                lbl_clientMsg.setText("you lost");
            }
            pnlCommends.removeAll();
            pnlCommends.revalidate();
            pnlCommends.repaint();
            this.revalidate();
            this.repaint();
            //end your turn
            dealerTurn();
        } else {
            lbl_clientMsg.setText("can't double after hit");
        }
    }
    
    private void btn_quitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_quitMouseClicked
        //quit table
        data.setClientMsg("quit game");
        data.setPlayer(player);                             //take the player class into the data class 
        
        server.createConnection();                          //connect to the server
        server.setObjectToServer(data);                     //send the data to the server
        data = (Data) server.getObjectFromServer();         //get the data back from the server
        server.closeConnection();                           //close connection with the server
        
        if (data.getAnswer() == 1) {                        //if you quit the game
            System.out.println("you quit table number - " + player.getGameId() + " -");
            table = new Table();
            player = data.getPlayer();
            
            //go back to lobbyScreen
            LobbyScreen lobbyScreen = new LobbyScreen();
            lobbyScreen.player = this.player;
            lobbyScreen.server = this.server;
            
            lobbyScreen.setVisible(true);
            this.dispose();
        } else {
            lbl_clientMsg.setText("failed to quit the table");
        }
    }//GEN-LAST:event_btn_quitMouseClicked
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TableScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TableScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TableScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TableScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TableScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_quit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbl_clientMsg;
    private javax.swing.JLabel lbl_coins;
    private javax.swing.JLabel lbl_tableNumber;
    private javax.swing.JPanel pnlBet;
    private javax.swing.JPanel pnlCommends;
    private javax.swing.JPanel pnlDealer;
    private javax.swing.JPanel pnlNewGame;
    private javax.swing.JPanel pnlPlayer;
    private javax.swing.JScrollPane spnl_dealer;
    private javax.swing.JScrollPane spnl_player;
    // End of variables declaration//GEN-END:variables
}
