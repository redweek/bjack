package bjackclient;

import classeslibrary.Data;
import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author VERED
 */
public class ServerConnection {
    
    private Socket socketOfClient;
    private ObjectInputStream objIn;
    private ObjectOutputStream objOut;
    private DataInputStream reader;
    private DataOutputStream cout;
    
    public void createConnection(){
        
        try {
            socketOfClient = new Socket("localhost",6666);
            reader = new DataInputStream(socketOfClient.getInputStream());
            cout = new DataOutputStream(socketOfClient.getOutputStream());
        } catch (IOException ex) {
            System.out.println("error in the connection to server");
            System.out.println("exeption : " + ex);
        }
    }
    
    public void closeConnection(){
        
        try {
            reader.close();
            cout.close();
            socketOfClient.close();
        } catch (IOException ex) {
            System.out.println("error in closing connection with the server");
            System.out.println("exeption : " + ex);
        }
    }
    
    public Object getObjectFromServer(){
        Object obj = null;
        try {
            objIn = new ObjectInputStream(socketOfClient.getInputStream());
            obj = objIn.readObject();
        } catch (IOException  | ClassNotFoundException ex) {
            System.out.println("error in getting Object from server" + ex);
            closeConnection();
        }
        return obj;
    }
    
    public String getStringFromServer(){
        
        String str;
        try{
            str = reader.readUTF();
        }catch(IOException ex){
            str = "-1";
            closeConnection();
            System.out.println("error in getting string from client");
        }
        return str;
    }
    
    public int getIntFromServer(){
        
        int num;
        try{
            num = reader.readInt();
        }catch(IOException ex){
            num = -1;
            closeConnection();
            System.out.println("error in getting int from client");
        }
        return num;
    }
    //func to send obj to server
    public void setObjectToServer(Object obj){
        try {
            objOut = new ObjectOutputStream(socketOfClient.getOutputStream());
            objOut.writeObject(obj);
            objOut.flush();
        } catch (IOException ex) {
            System.out.println("trouble in sending object to the server" + ex);
            closeConnection();
        }
        
    }
    
    public void setStringToServer(String str){
        
        try {
            cout.writeUTF(str);
            cout.flush();
        } catch (IOException ex) {
            System.out.println("error in setting String into the server");
            closeConnection();
        }
    }
    
    public void setIntToServer(int num){
        
        try {
            cout.writeInt(num);
            cout.flush();
        } catch (IOException ex) {
            System.out.println("error in setting int into the server");
            closeConnection();
        }
        
    }
}
