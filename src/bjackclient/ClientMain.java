/*
    this class init the client side
    client forms and client logic
    with connection to the server
*/
package bjackclient;

import BjackForms.OpenScreen;
import classeslibrary.*;
import java.util.Scanner;

/*
 * @author red week
 */
public class ClientMain {

    public static void main(String[] args) {

        OpenScreen openScreen = new OpenScreen();
        openScreen.setVisible(true);
        
        //test();   //test function
    }
    
    //test func to check things 'on the paper' before lunch 
    public static void test() {

        Scanner input = new Scanner(System.in);                 //scanners for test
        Scanner input1 = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);
        Scanner numb = new Scanner(System.in);                  
        ServerConnection server = new ServerConnection();
        Data data = new Data();
        Player player = new Player();
        PlayerRecord guest = new PlayerRecord();
        Table table;
        Statistics statistics = new Statistics();
        int gate = 1;
        
        server.createConnection();                              //connection to the server
        while (gate != 0) {
            //get choice of player for test
            System.out.println("what would you like to do : "); //menu for test
            System.out.println("#1 - register");
            System.out.println("#2 - log in");
            System.out.println("#3 - guest");
            System.out.println("close");

            String choice = input.nextLine();
            data.setClientMsg(choice);                          //write the choice in the data class
            ////////////////////////////////
            String password;
            int ticket = 0;

            switch (choice) {
                case "register":                                  //if the coice is log in

                    System.out.println("enter name");               //take the name
                    player.setName(input.nextLine());

                    while (true) {
                        System.out.println("enter password");
                        System.out.println("8 charecters or more");
                        password = input.nextLine();
                        if (password.length() >= 8) {
                            break;
                        }else{
                            System.out.println("not enough charecters");
                        }
                    }
                    player.setPassword(password);

                    data.setPlayer(player);                             //take the player class into the data class 
                    server.setObjectToServer(data);                     //send the data to the server
                    data = (Data) server.getObjectFromServer();         //get the object from the server back to obj
                    player = data.getPlayer();                          //fill the player with the right data (id and amount)
                    
                    //System.out.println("you have got " + player.getAmount() + " coins  ;)");
                    if (player.getId() == -1) {
                        System.out.println("user already exist");              //explain it self
                    } else {
                        System.out.println("success, player created");
                        ticket = 1;
                    }
                    break;
                case "log in":

                    System.out.println("enter name");               //take the name
                    player.setName(input.nextLine());

                    System.out.println("enter password");           //take password
                    player.setPassword(input.nextLine());

                    data.setPlayer(player);                         //take the player class into the data class
                    server.setObjectToServer(data);                 //send the data to the server
                    data = (Data) server.getObjectFromServer();     //get the object from the server back to obj
                    player = data.getPlayer();                      //fill the player with the right data (id and amount)
                    
                    switch (player.getId()) {
                        case -1:
                            System.out.println("name or password are not correct");     //explain it self
                            break;
                        case -2:
                            System.out.println("you are being blocked for 30 minutes");
                            break;
                        case -3:
                            System.out.println("you are blocked try later ha ha");
                            break;
                        default:
                            System.out.println("success");
                            System.out.println("you are in");   //in case of success return in the answer the id of the player
                            ticket = 1;
                            break;
                    }
                    break;
                case "guest":
                    // IN CONSTRACTION
                    guest.setName("guest");
                    
                    data.setPlayer(player);                             //take the player class into the data class 
                    server.setObjectToServer(data);                     //send the data to the server
                    data = (Data) server.getObjectFromServer();         //get the object from the server back to obj
                    player = data.getPlayer();                          //fill the player with the right data (id and amount)
                    
                    //System.out.println("you have got " + player.getAmount() + " coins  ;)");
                    if (player.getId() == -1) {
                        System.out.println("user already exist");              //explain it self
                    } else {
                        System.out.println("success, player created");
                        ticket = 1;
                    }
                    break;
                default:
                    System.out.println("bye bye");
                    gate = 0;
                    break;
            }
            if (1 == ticket) {
                while (gate != 0) {
                    //get choice of player for test
                    System.out.println("what would you like to do : ");     //if user got in take him to the lobby
                    System.out.println("#1 - play");
                    System.out.println("#2 - statistics");
                    System.out.println("# - back");
                    //System.out.println("***enter some number pleace");
                    
                    String choice1 = input.nextLine();
                    data.setClientMsg(choice1);
                    ///////////////////////////////
                    switch (choice1) {
                        case "play":
                            
                            data.setPlayer(player);                             //take the player class into the data class 
                            server.setObjectToServer(data);                     //send the data to the server
                            data = (Data) server.getObjectFromServer();         //get the object from the server back to obj
                            table = data.getTable();
                            if (table.getId() > 0) {                             //if you are in a game
                                player = data.getPlayer();
                                System.out.println("you are in table number - " + player.getGameId() + " -");
                                
                                while(gate != 0){
                                    //check server massage
                                    
                                    if ("bet".equals(data.getServerMsg())){
                                        int betGate = 1;
                                        System.out.println("you have got " + player.getAmount() + " coins  ;)");
                                        while (betGate != 0) {                              //loop to get the bet
                                            System.out.println("place your bet please:");
                                            int bet = input.nextInt();
                                            if (player.firstBet(bet) == 1) {                //update the first bet
                                                betGate = 0;
                                            }else{
                                                System.out.println("you dont have enough money");
                                            }
                                        }
                                        
                                        data.setClientMsg("bet");                           //tell the server to take the bet
                                        data.setPlayer(player);                             //take the player into the data
                                        data.setTable(table);                               //take the table into the data
                                        server.setObjectToServer(data);                     //send the data to the server
                                        data = (Data) server.getObjectFromServer();         //get the data from the server back
                                        //player and dealer gets their first cards
                                        player = data.getPlayer();
                                        table = data.getTable();
                                        
                                        player.setFirstHand(player.cardsSum());         //update the first bet
                                        System.out.println("your cards is :");          //print the player cards
                                        for(int index = 0; index < player.getCards().size(); index++){
                                            System.out.print(player.getCard(index).getName() + ", ");
                                        }
                                        System.out.println("");
                                        System.out.println("the dealer cards is :");    //print the dealer card
                                        System.out.println(table.getDealerCard(0).getName() + ", XX");
                                        
                                        
                                        
                                        if(player.checkBjack() == 1){                   //if Bjack
                                            System.out.println("Bjack! you won!!!");
                                            //get the money to the player
                                            player.Bjack();
                                            //update Bjack and win in the statistic
                                            data.setClientMsg("Bjack");
                                            data.setPlayer(player);
                                            data.setTable(table);                           //set the table in data for the hand insert
                                            server.setObjectToServer(data);                 //send the data to the server
                                            data = (Data) server.getObjectFromServer();     //get the data from the server back
                                            player = data.getPlayer();
                                            table = data.getTable();
                                        }else{
                                            while(gate != 0){
                                                //get choice of player for test
                                                System.out.println("what would you like to do : ");
                                                System.out.println("#1 - pass");
                                                System.out.println("#2 - one more card");
                                                if(player.getCards().size() == 2 && player.getAmount() >= player.getFirstBet()){          //if player got just 2 cards
                                                    System.out.println("#3 - double");
                                                }
                                                System.out.println("# - quit game");
                                                //System.out.println("***enter some number pleace");
                                                //x = numb.nextInt();
                                                String choice2 = input1.nextLine();
                                                data.setClientMsg(choice2);
                                                ///////////////////////////////
                                                switch(choice2){
                                                    case "pass":
                                                        player.setFinalHand(player.cardsSum());
                                                        player.setFinalBet(player.getFirstBet());
                                                        player.setStatus("done");
                                                        
                                                        gate = 0;
                                                        break;
                                                    case "one more card":
                                                        player.setFinalBet(player.getFirstBet());       //set the final bet
                                                        data.setPlayer(player);                         //take the player into the data
                                                        data.setTable(table);                           //take the table into the data
                                                        server.setObjectToServer(data);                 //send the data to the server
                                                        data = (Data) server.getObjectFromServer();     //get the data from the server back
                                                        //player gets the card
                                                        player = data.getPlayer();
                                                        table = data.getTable();
                                                        
                                                        player.setFinalHand(player.cardsSum());         //update the final hand
                                                        player.setStatus("done");
                                                        System.out.println("your cards is :");          //print the player cards
                                                        for(int index = 0; index < player.getCards().size(); index++){
                                                            System.out.print(player.getCard(index).getName() + ", ");
                                                        }System.out.println("");
                                                        
                                                        if(player.cardsSum() > 21){         //if player pass the 21 points
                                                            System.out.println("you lost");
                                                            player.lose();
                                                            data.setClientMsg("lose");
                                                            data.setPlayer(player);
                                                            data.setTable(table);                           //set the table in data for the hand insert
                                                            server.setObjectToServer(data);                 //send the data to the server
                                                            data = (Data) server.getObjectFromServer();     //get the data from the server back
                                                            player = data.getPlayer();
                                                            table = data.getTable();
                                                            gate = 0;
                                                        }
                                                        break;
                                                    case "double":
                                                        
                                                        data.setPlayer(player);                         //take the player into the data
                                                        data.setTable(table);                           //take the table into the data
                                                        server.setObjectToServer(data);                 //send the data to the server
                                                        data = (Data) server.getObjectFromServer();     //get the data from the server back
                                                        
                                                        player = data.getPlayer();                      //player gets the card
                                                        table = data.getTable();
                                                        player.doubleBet();
                                                        
                                                        System.out.println("your cards is :");          //print the player cards
                                                        for(int index = 0; index < player.getCards().size(); index++){
                                                            System.out.print(player.getCard(index).getName() + ", ");
                                                        }System.out.println("");
                                                        
                                                        if(player.cardsSum() > 21){     //if player pass the 21 points
                                                            System.out.println("you lost");
                                                            player.lose();
                                                            player.setFinalBet(player.getFinalBet() * 2);
                                                            data.setClientMsg("lose");
                                                            data.setPlayer(player);
                                                            data.setTable(table);                           //set the table in data for the hand insert
                                                            server.setObjectToServer(data);                 //send the data to the server
                                                            data = (Data) server.getObjectFromServer();     //get the data from the server back
                                                            player = data.getPlayer();
                                                            table = data.getTable();
                                                        }
                                                        gate = 0;
                                                        break;
                                                    case "guit game":
                                                        data.setPlayer(player);                             //take the player class into the data class 
                                                        server.setObjectToServer(data);                     //send the data to the server
                                                        data = (Data) server.getObjectFromServer();         //get the object from the server back to obj
                                                        if (data.getAnswer() == 1) {                        //if you quit the game
                                                            System.out.println("you quit table number - " + player.getGameId() + " -");
                                                            table = new Table();
                                                            player = data.getPlayer();
                                                            gate = 0;
                                                        } else {
                                                            System.out.println("failed to quit the table");
                                                        }                                                                                                        
                                                        break;
                                                    default:
                                                        data.setClientMsg("quit game");
                                                        data.setPlayer(player);                                //take the player class into the data class 
                                                        server.setObjectToServer(data);                        //send the data to the server
                                                        data = (Data) server.getObjectFromServer();            //get the object from the server back to obj
                                                        if (data.getAnswer() == 1) {                             //if you quit the game
                                                            System.out.println("you quit table number - " + player.getGameId() + " -");
                                                            table = new Table();
                                                            player = data.getPlayer();
                                                        }else{
                                                            System.out.println("failed to quit the table");
                                                        }
                                                        System.out.println("bye bye");
                                                        gate = 0;        
                                                        break;
                                                }
                                            }
                                            gate = 1;     //of game
                                        }
                                        if(table.getDealerCards().size() > 0){
                                            System.out.println("the dealer cards is :");          //print the dealer cards
                                            for(int index = 0; index < table.getDealerCards().size(); index++){
                                                System.out.print(table.getDealerCard(index).getName() + ", ");
                                            }System.out.println("");
                                        }
                                        if("done".equals(player.getStatus())){    
                                            if(table.cardsSum() == 21){     //if dealer got Bjack 
                                                //everyone losses
                                                System.out.println("Bjack! dealer won!! you lost!!!");
                                                //take the money from the player
                                                data.setClientMsg("lose");
                                                data.setPlayer(player);
                                                data.setTable(table);                           //set the table in data for the hand insert
                                                server.setObjectToServer(data);                 //send the data to the server
                                                data = (Data) server.getObjectFromServer();     //get the data from the server back
                                                player = data.getPlayer();
                                                table = data.getTable();
                                            }else{
                                                while(table.cardsSum() < 17){
                                                    //dealer gets more cards
                                                    data.setClientMsg("get dealer card");
                                                    data.setTable(table);
                                                    server.setObjectToServer(data);
                                                    data = (Data) server.getObjectFromServer();
                                                    table = data.getTable();
                                                    System.out.println("the dealer cards is :");          //print the dealer cards
                                                    for(int index = 0; index < table.getDealerCards().size(); index++){
                                                        System.out.print(table.getDealerCard(index).getName() + ", ");
                                                    }System.out.println("");
                                                }
                                                if (table.cardsSum() > 21){
                                                    //dealer lose everyone wins
                                                    System.out.println("dealer lose");
                                                    System.out.println("you won!!!");
                                                    player.win();
                                                    data.setClientMsg("win");
                                                    data.setPlayer(player);
                                                    data.setTable(table);
                                                    server.setObjectToServer(data);
                                                    data = (Data) server.getObjectFromServer();
                                                    player = data.getPlayer();
                                                    table = data.getTable();
                                                }else{
                                                    //round over check results
                                                    if(table.cardsSum() > player.cardsSum()){
                                                        //dealer high then player = player lose
                                                        System.out.println("dealer won!!!");
                                                        System.out.println("you lost!!!");
                                                        data.setClientMsg("lose");
                                                        data.setPlayer(player);
                                                        data.setTable(table);                           //set the table in data for the hand insert
                                                        server.setObjectToServer(data);                 //send the data to the server
                                                        data = (Data) server.getObjectFromServer();     //get the data from the server back
                                                        player = data.getPlayer();
                                                        table = data.getTable();
                                                    }else if(table.cardsSum() < player.cardsSum()){
                                                        //player high then tealer = player win
                                                        System.out.println("dealer lost!!!");
                                                        System.out.println("you won!!!");
                                                        player.win();
                                                        data.setClientMsg("win");
                                                        data.setPlayer(player);
                                                        data.setTable(table);                           //set the table in data for the hand insert
                                                        server.setObjectToServer(data);                 //send the data to the server
                                                        data = (Data) server.getObjectFromServer();     //get the data from the server back
                                                        player = data.getPlayer();
                                                        table = data.getTable();
                                                    }else{
                                                        //even = player get his money back
                                                        System.out.println(" you are even ");
                                                        player.even();
                                                        data.setClientMsg("even");
                                                        data.setPlayer(player);
                                                        data.setTable(table);                           //set the table in data for the hand insert
                                                        server.setObjectToServer(data);                 //send the data to the server
                                                        data = (Data) server.getObjectFromServer();     //get the data from the server back
                                                        player = data.getPlayer();
                                                        table = data.getTable();
                                                    }
                                                }
                                            }
                                        }
                                        
                                        System.out.println("round is over");
                                        player.resetRound();            //reset the round info to the player
                                        table.resetDealerCards();       //reset the dealer cards
                                        
                                        //check if player want another round
                                        System.out.println("would you like to continue -  yes | no");
                                        String choice3 = input2.nextLine();
                                        data.setClientMsg(choice3);
                                        ////////////////////////////////////
                                        if("yes".equals(choice3)){
                                            data.setClientMsg("play");      
                                            data.setTable(table);
                                            server.setObjectToServer(data);                 //send the data to the server
                                            data = (Data) server.getObjectFromServer();     //get the data from the server back
                                            table = data.getTable();
                                        }else{
                                            data.setClientMsg("quit game");
                                            data.setPlayer(player);                                //take the player class into the data class 
                                            server.setObjectToServer(data);                        //send the data to the server
                                            data = (Data) server.getObjectFromServer();            //get the object from the server back to obj
                                            if (data.getAnswer() == 1) {                             //if you quit the game
                                                System.out.println("you quit table number - " + player.getGameId() + " -");
                                                table = new Table();
                                                player = data.getPlayer();
                                            }else{
                                                System.out.println("failed to quit the table");
                                            }
                                            System.out.println("bye bye");
                                            gate = 0;
                                        }
                                    }else{
                                        System.out.println("wait till the end of the round..");
                                        data = (Data) server.getObjectFromServer();     //wait to your turn
                                    } 
                                }
                                gate = 1;   //of lobby
                            }else{
                                System.out.println("failed to get into the table");
                            }
                            break;
                        case "statistics":
                            statistics.setPlayerId(player.getId());
                            data.setStatistics(statistics);                     //set the statistics class into the data class
                            //server.createConnection();
                            server.setObjectToServer(data);                     //send the data to the server
                            data = (Data) server.getObjectFromServer();         //get the object from the server back to obj
                            //server.closeConnection();
                            statistics = data.getStatistics();                  //get the statistics back from data
                            //check if server didn't got the stat
                            if(statistics.getPlayerId() == 0){
                                System.out.println("error in getting the needed info");
                            }else{
                                //print the statistics
                                System.out.println("the statistics of - " + player.getName());
                                System.out.println("total wins : " + statistics.getWins());
                                System.out.println("total losses : " + statistics.getLosses());
                                System.out.println("most money held : " + statistics.getMaxSum());
                                System.out.println("correct amount of money : " + player.getAmount());
                            }
                            break;
                        case "back":
                            gate = 0;
                            //get to the open screen
                            break;
                        default:
                            System.out.println("bye bye");
                            gate = 0;
                            break;
                    }
                }
                gate = 1;       //of open screen
            }
        }
        data.setClientMsg("close");
        server.setObjectToServer(data);
        server.getObjectFromServer();
        server.closeConnection();

    }
}
