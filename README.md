# Bjack

project of Bjack game

link to the bak of the DB : https://drive.google.com/open?id=1WM9EmRyIkg5gyBXVozCOh-6V1FAlZi_w

with client end server
its a card game wite 1-3 players who playes vs the dealer(server)

tech :

java - object oriented, swing forms
sokcet

msSql for the DB with stored procedures

rules :

every player bet on money and get 2 cards which everyone can see
and the dealer get one card open and one closed
the point of the game is to get more points from the dealer without passing the 21 points

every player in his turn decide to get more cards/double the money and get just one more final card/pass the turn

if player pass the 21 points in his card he losses

when everyone are finished there turns the dealer open the close card
and if he is got less then 17 he keeps taking more cards

and then checking if the player got more points then the dealer or the dealer got more points then 21
the player win and get double money then he pay
if they even in there points the player get his money back
else the player lost his money

spicial cases - *Bjack = when the player get two cards one of aces and one that worth 10
                        he gets what he payed * 2.5 on the spot
                *every card big the 10 is worth 10
                *ace can be worth 1 if the player got more then 21

list of project:

name : BjackClasses
definition : contains the classes for the project

name : BjackClient
definition : contains the swing forms to show to the client and functions to
             connect with the server

name : BjackServer
definition : contains functions to connect with the player and fonctions to 
             connect with the DB and the main part of the game logics